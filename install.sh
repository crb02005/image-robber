APP_NAME="imageRobber"
APP_EXEC_LOCATION="/usr/local/"

if [ -d "$APP_EXEC_LOCATION" ]; then
    echo "Copying to $APP_EXEC_LOCATION"
    #mkdir $APP_EXEC_LOCATION
    sudo chmod +x ./bin/*
    sudo cp -r ./bin $APP_EXEC_LOCATION
fi

USER_SETTING_DIRECTORY="$HOME/.$APP_NAME/"

if [ ! -d "$USER_SETTING_DIRECTORY" ]; then
    echo "Creating settings directory $USER_SETTING_DIRECTORY"
    mkdir $USER_SETTING_DIRECTORY
    cp ./settings/* $USER_SETTING_DIRECTORY
fi
