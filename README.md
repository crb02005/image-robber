# ImageRobber

This is a set of scripts to help snagging images

## Requirements

### python3.7

if you don't have this installed here: 

```
#!/usr/bin/python3.7
```

you will have a bad time

### xdotool

This gets the mouse location.

### image_magick

used for capturing images

### tesseract

ocring

## What the scripts do?

### install.sh

makes a ~/.imageRobber/settings.json
copies scripts to /usr/bin/

### jread 

Reads a JSON key from file. It only works on a flat dictionary.

```
jread <file> <key>
```

### jwrite

Writes a JSON key to file. It only works on a flat dictionary.

```
jwrite <file> <key> <value>
```

### store_mouse_location

Saves the mouse x/y in settings.json

```
store_mouse_location <position (0|1)>
```

### import_expression_builder

```
import_expression_builder <x0> <x1> <y0> <y1>
```

gets the expression for import

### capture_screen_shot

captures a subset of the screen from settings

```
import -window root -crop <expression from x0 x1 y0 y1 from settings> <date stamp>.<extension from settings>"
```

## How to use

keyboard bind "store_mouse_location 0" to a key command
keyboard bind "store_mouse_location 1" to a key command
keyboard bind "capture_screen_shot" to a key command

Enable OCR on output
keyboard bind "jwrite $HOME/.imageRobber/settings.json "convert to text" 1

Disable OCR on output
keyboard bind "jwrite $HOME/.imageRobber/settings.json "convert to text" 0

